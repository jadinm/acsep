# Update epl-scientific-staff

## Setup configuration

`python` and `pip` must be available in PATH.

Linux and MacOS users can use the Makefile to launch the server locally.
No support is provided for Windows.

## Start the server

Run `make` to start the server and download dependencies

You can now enter in your browser the url *http://127.0.0.1:8000* to get to the application.

## Use the application

The first page shows the current list of actions to take. At first, nothing is shown since the application did not interact with the SYMPA server yet.

The second page enables you to enter your UCL credentials to connect to the SYMPA server and retrieve the subscribed members of the list. The extraction process is **really** slow (~3 min), even with a good internet connection.

## TODO if the application crashes in the middle of the process

Try to rerun the program and let it run to the end.

#### If it doesn't work

The data source **EPL members** at [page](https://sympa-2.sipr.ucl.ac.be/sympa/edit_list_request/epl-scientific-staff/data_source) must be updated. Update the filter parameter to *(&(uclmemberof=mbr-sst-epl)(!(uclmemberof=mbr-sst-epl)))* and click on the button at the bottom of this page.

#### Why ?

To retrieve all the potential mail addresses, the app uses a preset LDAP request at this
[page](https://sympa-2.sipr.ucl.ac.be/sympa/edit_list_request/epl-scientific-staff/data_source). Usually the asociated filter returns nothing (see the filter above) but, during the extraction, it is set to get all members of both the EPL and one of the associated institutes. Therefore, during a few seconds, there are many more members to the mailing list than planned and it could stay like this in case of crash.

Actually, it should only happen if the application is killed or if the connection is lost.

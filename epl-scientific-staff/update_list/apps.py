from django.apps import AppConfig


class UpdateListConfig(AppConfig):
    name = 'update_list'

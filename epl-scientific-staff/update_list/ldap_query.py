# -*- coding: utf-8 -*-
import cookielib

import mechanize
from bs4 import BeautifulSoup
from django import forms
from django.urls import reverse
from django.views.generic import FormView

from models import EPLMember


class LDAPForm(forms.Form):
	username = forms.CharField(label='UCL username', max_length=100)
	password = forms.CharField(widget=forms.PasswordInput(), label='UCL password', max_length=100)


class LDAPQuery(FormView):
	template_name = "ldap_query.html"
	form_class = LDAPForm

	def get_success_url(self):
		return reverse("show_diff")

	def form_valid(self, form):
		"""
		Connect to mailing list administration.
		Then, extract all the the members of the EPL.
		Only keep the teaching assistants and the EPL members.
		"""
		br = mechanize.Browser()

		# Cookie Jar
		cj = cookielib.LWPCookieJar()
		br.set_cookiejar(cj)

		# Browser options
		br.set_handle_equiv(True)
		br.set_handle_gzip(True)
		br.set_handle_redirect(True)
		br.set_handle_referer(True)
		br.set_handle_robots(False)

		# Follows refresh 0 but not hangs on refresh > 0
		br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

		# Want debugging messages?
		br.set_debug_http(False)
		br.set_debug_redirects(False)
		br.set_debug_responses(False)

		# User-Agent (this is cheating, ok?)
		br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1)'
		                                ' Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

		try:
			r = br.open('https://sympa-2.sipr.ucl.ac.be/sympa/my')
			r.read()

			# Go to the Shibboleth login form
			for sympa_form in br.forms():
				if sympa_form.attrs.get('id') == 'use-sso':
					br.form = sympa_form
					break
			br.submit()

			# Handle Shibboleth authentication
			br.select_form(nr=0)
			br.form['j_username'] = form.cleaned_data['username']
			br.form['j_password'] = form.cleaned_data['password']
			br.submit()

			br.select_form(nr=0)
			br.submit()

			print("We connected to SYMPA 2 server")

			br.follow_link(text_regex="epl-scientific-staff.*")

			br.follow_link(text="Admin")

			# Activate epl-member data source
			req = br.click_link(Link("https://sympa-2.sipr.ucl.ac.be/sympa/edit_list_request/epl-scientific-staff/data_source"))
			br.open(req)
			br.select_form(nr=1)
			br.form['single_param.include_ldap_query.0.filter'] = "(&(uclmemberof=mbr-sst-epl)" \
			                                                      "(|(uclmemberof=mbr-sst-ictm)(uclmemberof=mbr-sst-immc)" \
			                                                      "(uclmemberof=mbr-sst-imcn)))"
			br.submit()

			try:
				# List all members here !
				req = br.click_link(Link("https://sympa-2.sipr.ucl.ac.be/sympa"
				                         "?sortby=email&action=review&list=epl-scientific-staff&size=500"))
				response = br.open(req)

				email_list = []
				soup = BeautifulSoup(response.read(), "html5lib")
				table_suscribers = soup.find('table')
				for row in table_suscribers.findAll('tr'):
					cells = row.findAll('td')
					if len(cells) > 0:
						sources = [source.strip() for source in cells[5].contents if getattr(source, 'name', None) != 'br']
						email_list.append((row.find('input').attrs["value"], sources))

				print("All the potential emails addresses were extracted !")
			finally:
				# Deactivate epl-member data source
				req = br.click_link(Link("https://sympa-2.sipr.ucl.ac.be/sympa/edit_list_request/epl-scientific-staff/data_source"))
				br.open(req)
				br.select_form(nr=1)
				br.form['single_param.include_ldap_query.0.filter'] = "(&(uclmemberof=mbr-sst-epl)(!(uclmemberof=mbr-sst-epl)))"
				br.submit()
		finally:
			# Logout
			br.select_form(nr=0)
			br.submit()
			print("The state of the SYMPA server was cleaned")

		# Flush database
		EPLMember.objects.all().delete()

		# Get Infos for each email address
		print("Going through the directory of the UCL to get their job title\nThis operation takes time !")
		for email, sources in email_list:
			try:
				response = br.open('https://uclouvain.be/%s' % email.split("@")[0])
				soup = BeautifulSoup(response.read(), "html5lib")
				job_titles = [job.strip() for job in soup.find('span', {'itemprop': 'jobTitle'}).contents
				              if getattr(job, 'name', None) != 'br']
				# Given name and family names are inverted because of the champions of the UCL
				member = EPLMember.objects.get_or_create(email=email)[0]
				member.family_name = soup.find('span', {'itemprop': 'givenName'}).text
				member.given_name = soup.find('span', {'itemprop': 'familyName'}).text
				member.job_titles = " & ".join(job_titles)
				member.sources = " & ".join(sources)
				member.save()
				print("Member added: %s" % member.full_str())
			except mechanize.HTTPError:
				member = EPLMember.objects.get_or_create(email=email)[0]
				member.sources = " & ".join(sources)
				member.save()
				print("email %s not found => This member left !" % email)

		print("All the potential members of the mailing list were categorized !")
		return super(LDAPQuery, self).form_valid(form)


class Link:
	def __init__(self, url):
		self.absolute_url = url

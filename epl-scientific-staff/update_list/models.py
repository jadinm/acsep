# -*- coding: utf-8 -*-
from django.db import models


RETIRED = "retired"
ASSISTANT = ur"^[A|a]ssistante?$"
SUBSCRIBED = ur"^(?:[S|s]ubscribed)|(?:[A|a]bonn.*)$"


class EPLMember(models.Model):
	email = models.EmailField(unique=True)
	family_name = models.CharField(max_length=255, default="")
	given_name = models.CharField(max_length=255, default="")
	job_titles = models.CharField(max_length=255, default=RETIRED)  # Multiple jobs titles possible => separated by '\n'
	sources = models.CharField(max_length=255, default="")  # Multiple sources are possible => separated by '\n'

	def __str__(self):
		return "%s %s %s" % (self.email.encode("utf8"), self.given_name.encode("utf8"), self.family_name.encode("utf8"))

	def full_str(self):
		return "%s %s %s %s from %s" % (self.email.encode("utf8"), self.given_name.encode("utf8"),
		                                self.family_name.encode("utf8"), self.job_titles.encode("utf8"),
		                                self.sources.encode("utf8"))

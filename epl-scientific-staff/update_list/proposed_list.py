# -*- coding: utf-8 -*-
from django.views import generic

from update_list.models import EPLMember


class ProposedList(generic.ListView):
	model = EPLMember
	template_name = "proposed_list.html"

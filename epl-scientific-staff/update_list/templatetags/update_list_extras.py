# -*- coding: utf-8 -*-
import re

from django import template

from update_list.models import ASSISTANT, SUBSCRIBED, RETIRED

register = template.Library()


def _is_job_assistant(member):
	job_titles = member.job_titles.split(" & ")
	for job in job_titles:
		if re.match(ASSISTANT, job.encode("utf8")) is not None:
			return True
	return False


def _is_retired(member):
	job_titles = member.job_titles.split(" & ")
	for job in job_titles:
		if job == RETIRED:
			return True
	return False


def _is_subscribed(member):
	sources = member.sources.split(" & ")
	for source in sources:
		if re.search(SUBSCRIBED, source.encode("utf8")) is not None:
			return True
	return False


@register.filter
def non_subscribed_assistant(members):
	"""Keep only the assistants that are not yet subscribed to the mailing list"""
	return [member for member in members if _is_job_assistant(member) and not _is_subscribed(member)]


@register.filter
def subscribed_retired_assistant(members):
	"""Keep only the 'retired' subscribers and not yet removed from the mailing list"""
	return [member for member in members if _is_retired(member) and _is_subscribed(member)]


@register.filter
def subscribed_non_assistant(members):
	"""Keep only the subscribed email addresses that are not assistants"""
	return [member for member in members
	        if not _is_job_assistant(member) and _is_subscribed(member) and not _is_retired(member)]


@register.filter
def with_job(member):
	"""Show the job along with the normal str"""
	return str(member) + " : " + member.job_titles

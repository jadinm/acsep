from django.conf.urls import url

from update_list.ldap_query import LDAPQuery
from update_list.proposed_list import ProposedList


urlpatterns = [
	url(r'^ldap$', LDAPQuery.as_view(), name='ldap_query'),
	url(r'^$', ProposedList.as_view(), name='show_diff'),
]

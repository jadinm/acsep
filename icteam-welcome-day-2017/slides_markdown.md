

# There are institutes and faculties

  Institutes handles all research questions
  (e.g., bachelor reform)
    * **ICTEAM**
    * iMMC
    * IMCN

  Faculties handles all teaching questions
  (e.g., bachelor reform)
    * **EPL**
    * LOCI
    * SC

# You are represented at every level !

	![Image](english_simple_schema.pdf)

# Who is member of ACSEP ?

  1. Teaching assistants
  2. Any other researcher requesting
     the **EPL membership**

# EPL membership is only positive to you

  * **Your opinion** on teaching can be heard !
  * No constraint: no additional teaching,...

# ACSEP team 2017-2018
	TODO photos team

